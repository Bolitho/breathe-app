/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateNotificationPreferences = /* GraphQL */ `
  subscription OnCreateNotificationPreferences(
    $filter: ModelSubscriptionNotificationPreferencesFilterInput
  ) {
    onCreateNotificationPreferences(filter: $filter) {
      id
      userId
      notificationState
      notificationMode
      notificationOccurrence
      notificationTime
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const onUpdateNotificationPreferences = /* GraphQL */ `
  subscription OnUpdateNotificationPreferences(
    $filter: ModelSubscriptionNotificationPreferencesFilterInput
  ) {
    onUpdateNotificationPreferences(filter: $filter) {
      id
      userId
      notificationState
      notificationMode
      notificationOccurrence
      notificationTime
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const onDeleteNotificationPreferences = /* GraphQL */ `
  subscription OnDeleteNotificationPreferences(
    $filter: ModelSubscriptionNotificationPreferencesFilterInput
  ) {
    onDeleteNotificationPreferences(filter: $filter) {
      id
      userId
      notificationState
      notificationMode
      notificationOccurrence
      notificationTime
      createdAt
      updatedAt
      __typename
    }
  }
`;
