/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createNotificationPreferences = /* GraphQL */ `
  mutation CreateNotificationPreferences(
    $input: CreateNotificationPreferencesInput!
    $condition: ModelNotificationPreferencesConditionInput
  ) {
    createNotificationPreferences(input: $input, condition: $condition) {
      id
      userId
      notificationState
      notificationMode
      notificationOccurrence
      notificationTime
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const updateNotificationPreferences = /* GraphQL */ `
  mutation UpdateNotificationPreferences(
    $input: UpdateNotificationPreferencesInput!
    $condition: ModelNotificationPreferencesConditionInput
  ) {
    updateNotificationPreferences(input: $input, condition: $condition) {
      id
      userId
      notificationState
      notificationMode
      notificationOccurrence
      notificationTime
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const deleteNotificationPreferences = /* GraphQL */ `
  mutation DeleteNotificationPreferences(
    $input: DeleteNotificationPreferencesInput!
    $condition: ModelNotificationPreferencesConditionInput
  ) {
    deleteNotificationPreferences(input: $input, condition: $condition) {
      id
      userId
      notificationState
      notificationMode
      notificationOccurrence
      notificationTime
      createdAt
      updatedAt
      __typename
    }
  }
`;
