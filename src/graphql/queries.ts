/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getNotificationPreferences = /* GraphQL */ `
  query GetNotificationPreferences($id: ID!) {
    getNotificationPreferences(id: $id) {
      id
      userId
      notificationState
      notificationMode
      notificationOccurrence
      notificationTime
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const listNotificationPreferences = /* GraphQL */ `
  query ListNotificationPreferences(
    $filter: ModelNotificationPreferencesFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listNotificationPreferences(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        userId
        notificationState
        notificationMode
        notificationOccurrence
        notificationTime
        createdAt
        updatedAt
        __typename
      }
      nextToken
      __typename
    }
  }
`;
