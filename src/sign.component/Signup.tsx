import React, { Component } from 'react';
import { Auth } from 'aws-amplify';
import Response from '../components/reponse.component/Response';
 
 
 

interface SignUpProps {
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
    confirmpassword: string;
    handleVerification:  (message: { isSuccessSignUp: boolean, successMessage: string}) => void;
}

interface SignUpState   {
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
    confirmpassword: string;
    isLoading: boolean;
    errorMessage: string | null;
    successMessage: string | null;
    isSuccessSignUp:boolean;
    showSignUpScreen:boolean;
    
}

class SignUp extends Component<SignUpProps, SignUpState> {
    private hasError = false;
    private errorMessage:string|null= "";
    private successMessage:string|null="";
     
    constructor(props: SignUpProps) {
        super(props);
        this.state = {
            firstName: props.firstName || '',
            lastName: props.lastName || '',
            email: props.email || '',
            username: props.username || '',
            password: props.password || '',
            confirmpassword: props.confirmpassword || '',
            isLoading: false,
            errorMessage: null,
            successMessage: null,
            isSuccessSignUp:false,
            showSignUpScreen:true
        };
    }
  public  handleChange = (event:React.ChangeEvent<HTMLInputElement>)=>{
        const {name, value} = event.target;
        this.setState( previousState=>({...previousState, [name]:value}) as Pick<SignUpState, keyof SignUpState>);
    };
      
    
    private handleSignUp = async () => {
        const { firstName, lastName, email, username, password, confirmpassword } = this.state;
                  
        this.setState({ isLoading: true });
            
        try {
          this.validatePasswordMatch(password, confirmpassword);
          await Auth.signUp({
            username: username,
            password: password,
            attributes: {
              email: email,
              given_name: firstName,
              family_name: lastName,
            },
          });
         
          this.setState({isSuccessSignUp:true,showSignUpScreen:false,
            successMessage: 'Registration successful! Please check your email for verification.',
            errorMessage: null,
            
          }, ()=>{this.props.handleVerification({ isSuccessSignUp: this.state.isSuccessSignUp, successMessage: this.state.successMessage as string});});
       
          
          
          this.hasError = false;
        } catch (error) {
            
          if (error instanceof Error) {
            this.setState({showSignUpScreen:false, errorMessage: error.message, successMessage: null });
               
            this.hasError = true;
            this.errorMessage = error.message;
          }
        } finally {
          this.setState({ isLoading: false });
        }
      }
      
      private validatePasswordMatch(password: string, confirmPassword: string) {
        if (password !== confirmPassword) {
          throw new Error("The passwords do not match");
        }
      }
      
      
    
      render() {
        return this.renderSignUpScreen();
         
    }
    private onVerificationError = (errorMessage: string) => {
        this.setState({
            errorMessage: errorMessage,
           successMessage: null,
        });
        if(errorMessage){
            this.hasError=true;
        }
    }
    private renderSignUpScreen() {
        const { firstName, lastName, email, username, password, confirmpassword } = this.state;
      return  (
               
            <div className="form">
                {this.hasError&&(
                <Response
                    errorMessage={this.errorMessage}
                    hasError={this.hasError}
                    successMessage={this.successMessage}
                />
            )}
                <form>
                    <div className="form-group">
                        <label htmlFor="firstName">First Name</label>
                        <input
                            type="text"
                            className="form-control"
                            name="firstName"
                            value={firstName}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastName">Last Name</label>
                        <input
                            type="text"
                            className="form-control"
                            name="lastName"
                            value={lastName}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            className="form-control"
                            name="email"
                            value={email}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input
                            type="text"
                            className="form-control"
                            name="username"
                            value={username}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            className="form-control"
                            name="password"
                            value={password}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="confirmpassword">Confirm Password</label>
                        <input
                            type="password"
                            className="form-control"
                            name="confirmpassword"
                            value={confirmpassword}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className='form-group'>
                        <button type='button' id='signUp' className='button-custom button ' onClick={this.handleSignUp}>Sign Up</button>
                    </div>
                </form>
            </div>
        )
    }
    
    
}

export default SignUp;
