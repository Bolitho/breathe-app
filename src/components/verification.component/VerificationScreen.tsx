import { Auth } from "aws-amplify";
import React, { Component } from "react";
import "./VerificationScreen.scss";
import Response from "../reponse.component/Response";

interface VerificationScreenProps {
    isSignedUpSuccessfully: boolean;
    successMessageFromSignUp:string;
    
}

interface VerificationScreenState {
    isVerified: boolean;
    errorMessage: string | null;
    successMessage: string | null;
    verificationCode: string;
    username: string;
}

class VerificationScreen extends Component<VerificationScreenProps, VerificationScreenState> {
    constructor(props: VerificationScreenProps) {
        super(props);
        this.state = {
            isVerified: false,
            errorMessage: null,
            successMessage: null,
            verificationCode: "",
            username: "",
        };
    }

    handleVerification = async () => {
        const { verificationCode, username } = this.state;
        try {
            await Auth.confirmSignUp(username, verificationCode);
            this.setState({
                verificationCode: "",
                successMessage: "Verification successful",
                isVerified: true,
                errorMessage: null,
            });
        } catch (error) {
            if (error instanceof Error) {
                const errorMessage = error.message;
                this.setState({
                    errorMessage: errorMessage,
                    isVerified: false,
                    successMessage: null,
                });
                console.log(error.message);
            }
        }
    }

    render() {
        const {
            verificationCode,
        } = this.state;

        return (
            <div className="container  d-flex justify-content-center align-items-center vh-10">
                <div className="card">
                  
                    <div className="">
                        <h2 className="card-header-h2 d-flex justify-content-center">Verify Code</h2>
                    </div>
                    <div className=" mt-2 d-flex justify-content-center">
                        {this. shouldShowErrorMessage()? 
                             (
                                <Response
                                    errorMessage={this.state.errorMessage}
                                    hasError={true}
                                />
                            ): (<Response
                                
                                successMessage={this.props.successMessageFromSignUp}
                                hasError={ false}
                            />)
                        }
                           
                    </div>
                    <div className="card-body">
                        <form action="#">
                            <div className="form-group">
                                <label htmlFor="username">Enter username you used when creating the account</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    onChange={(event) =>
                                        this.setState({ username: event.target.value })
                                    }
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="verificationCode">Enter verification Code</label>
                                <input
                                    className="form-control"
                                    value={verificationCode}
                                    onChange={(event) =>
                                        this.setState({ verificationCode: event.target.value })
                                    }
                                />
                            </div>
                            <div className="form-group">
                                <button
                                    className="button button-custom"
                                    onClick={this.handleVerification}
                                >
                                    Verify
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
  private  shouldShowErrorMessage() {
         
        return this.state.errorMessage || (!this.props.isSignedUpSuccessfully && this.state.errorMessage);
    }
    
}

export default VerificationScreen;
