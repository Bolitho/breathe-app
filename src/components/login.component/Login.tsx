import React, { Component } from 'react';
import './Login.scss';
import { Auth } from 'aws-amplify';
import Response from '../reponse.component/Response';
 

interface LoginProps {
    username: string;
    password: string;
    handleLoginStatus:(isLoginedIn:boolean)=>void;
    
     
     
}

interface LoginState  {
    username: string;
    password: string;
    isLoading: boolean;
    errorMessage: string | null;
    successMessage: string | null;
    showLoginScreen: boolean;
}

class Login extends Component<LoginProps, LoginState> {
     
     
    

    constructor(props: LoginProps) {
        super(props);
        this.state = {
            username: props.username || '',
            password: props.password || '',
            isLoading: false,
            errorMessage: null,
            successMessage: null,
            showLoginScreen: true,
             
            
             
        };
    }

    private handleLogin = async () => {
        const { username, password } = this.state;

        this.setState({ isLoading: true });

        try {
            this.validateEntry(username, password);
            await Auth.signIn(username, password);
            this.props.handleLoginStatus(true);
            
            this.setState({ errorMessage: null, successMessage: 'Login successful!', showLoginScreen: false });
             
             
        } catch (error) {
            if (error instanceof Error) {
                console.log(error.message);
                this.setState({errorMessage: error.message, successMessage: null });
               
                 
            }
        } finally {
            this.setState({ isLoading: false });
        }
    };

    private validateEntry(username: string, password: string) {
        if (password === '' || username === '') {
            throw new Error('Username and password are required');
        }
    }

    public handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        this.setState(previousState => ({
            ...previousState,
            [name]: value,
        }));
    };

    private renderLoginScreen() {
        const { username, password } = this.state;

        return (
            <div>
                <form action="#" method="get" className="form">
                {this.state.errorMessage && (
                <Response
                    errorMessage={this.state.errorMessage}
                    hasError={true}
                />
            )}
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input
                            type="text"
                            className="form-control"
                            name="username"
                            value={username}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            className="form-control"
                            name="password"
                            value={password}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <button
                            type="button"
                            id="login"
                            className="button-custom button"
                            onClick={this.handleLogin}
                        >
                            Login
                        </button>
                    </div>
                </form>
            </div>
        );
        
    }

    render() {
         
            return this.renderLoginScreen();
         
       
    }
    
}

export default Login;
