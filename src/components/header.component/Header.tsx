import React from "react";
import { Link } from "react-router-dom";

import "./header.scss"

const Header: React.FC = () => {
    return (
        <header className="header">
            <nav className="header-nav">
                <div className="navbar-brand">
                    <Link to="/" className="navbar-logo"><h2>Breathe App</h2></Link>
                </div>
               
            </nav>
        </header>
    );
};

export default Header;
