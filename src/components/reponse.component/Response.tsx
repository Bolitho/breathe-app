import React, { Component } from "react";
import errorIcon from "./src/svg/error_FILL0_wght400_GRAD0_opsz48.svg";
import successIcon from "./src/svg/done_black_24dp.svg";

interface ResponseProps {
  errorMessage?: string|null;
  hasError: boolean;
  successMessage?: string|null;
}

interface ResponseState {
  hasError: boolean;
}

class Response extends Component<ResponseProps, ResponseState> {
  constructor(props: ResponseProps) {
    super(props);
    this.state = {
      hasError: props.hasError,
    };
  }

  render() {
    const { errorMessage, successMessage, hasError } = this.props;

    return (
        <div className="container">
        <div className="card">
          <div className={hasError ? "alert alert-danger d-flex justify-content-center" : "alert alert-success d-flex justify-content-center"}>
            <h2 className={hasError?"text-danger":"text-success"}>{hasError ? "Error" : "Success"}</h2>
            {hasError ? (
              <svg xmlns="http://www.w3.org/2000/svg"  fill="red" height="48" viewBox="0 -960 960 960" width="48"><path stroke="#ff0000"d="M479.982-280q14.018 0 23.518-9.482 9.5-9.483 9.5-23.5 0-14.018-9.482-23.518-9.483-9.5-23.5-9.5-14.018 0-23.518 9.482-9.5 9.483-9.5 23.5 0 14.018 9.482 23.518 9.483 9.5 23.5 9.5ZM453-433h60v-253h-60v253Zm27.266 353q-82.734 0-155.5-31.5t-127.266-86q-54.5-54.5-86-127.341Q80-397.681 80-480.5q0-82.819 31.5-155.659Q143-709 197.5-763t127.341-85.5Q397.681-880 480.5-880q82.819 0 155.659 31.5Q709-817 763-763t85.5 127Q880-563 880-480.266q0 82.734-31.5 155.5T763-197.684q-54 54.316-127 86Q563-80 480.266-80Zm.234-60Q622-140 721-239.5t99-241Q820-622 721.188-721 622.375-820 480-820q-141 0-240.5 98.812Q140-622.375 140-480q0 141 99.5 240.5t241 99.5Zm-.5-340Z"/></svg>
            ) : (
                <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#ffffff"><path stroke="#00ff00" d="M0 0h24v24H0V0z" fill="none"/><path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/></svg>
            )} </div>
            <div className="d-flex justify-content-center">
            <h3 className={hasError?"text-danger mt-2":" mt-2 text-success"}>{hasError ? errorMessage : successMessage}</h3>
          </div>
        </div>
         
      </div>
      
    );
  }
}

export default Response;
