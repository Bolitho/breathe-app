import React, { Component } from 'react';
import Login from '../login.component/Login';
import SignUp from '../../sign.component/Signup';
import './authentication.scss';
import NotificationScreen from '../notification.component/NotificationScreen';
 
import VerificationScreen from '../verification.component/VerificationScreen';

interface AuthenticationScreenState {
    isLoginMode: boolean;
    isLogined: boolean;
    handleLoginStatus: boolean;
    handleVerification: boolean;
    successMessage: string;
    signUPscreen:boolean;
}

interface AuthenticationScreenProps {
    handleLoginStatus: (isLogined: boolean) => void;
}

class AuthenticationScreen extends Component<AuthenticationScreenProps, AuthenticationScreenState> {

    constructor(props: AuthenticationScreenProps) {
        super(props);
        this.state = {
            isLoginMode: true,
            isLogined: false,
            handleLoginStatus: false,
            handleVerification: false,
            successMessage: "",
            signUPscreen:false
        };

        this.handleLoginStatus = this.handleLoginStatus.bind(this);
    }

    private handleLoginStatus = (isLogined: boolean) => {
        if (isLogined) {
            this.setState({handleLoginStatus:isLogined, isLoginMode: false ,signUPscreen:false});
        }
    }

    private handleVerification = (message: { isSuccessSignUp: boolean, successMessage: string}) => {
        this.setState({
            handleVerification: message.isSuccessSignUp,
            isLoginMode: false,
            successMessage: message.successMessage, signUPscreen:false
        });
    }

    toggleMode = () => {
        this.setState((prevState) => ({
            isLoginMode: !prevState.isLoginMode,
        }));
    };

    render() {
        

        if (this.state.isLoginMode&&!this.state.successMessage) {
            return (
                <div className="container  d-flex justify-content-center align-items-center vh-100">
                    <div className="authentication-screen card">
                        <div className="card-header-h2 d-flex justify-content-center">
                            <h2 className="authentication-screen-head">
                                Login Form
                            </h2>
                        </div>
                        <div className="card-body">
                            <Login handleLoginStatus={this.handleLoginStatus} username="" password="" />
                        </div>
                        <div className="button-container">
                            <p className="text-primary toggle-button" onClick={this.toggleMode}>
                                <span className="button-text">
                                    Don't have an account? Sign Up
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            );
        }

        if (this.state.handleLoginStatus) {
            return <NotificationScreen />;
        }

        if (this.state.handleVerification) {
            return (
                <div className='container d-flex justify-content-center align-items-center vh-100'>
                    
                    <VerificationScreen  isSignedUpSuccessfully={this.state.signUPscreen} successMessageFromSignUp={this.state.successMessage}/>
                        
                   
                </div>
            );
        }

        return (
            <div className="container  d-flex justify-content-center align-items-center vh-100">
                <div className="authentication-screen card">
                    <div className="card-header-h2 d-flex justify-content-center">
                        <h2 className="authentication-screen-head">
                            Sign Up Form
                        </h2>
                    </div>
                    <div className="card-body">
                        <SignUp
                         handleVerification={this.handleVerification}
                            firstName=""
                            lastName=""
                            email=""
                            username=""
                            password=""
                            confirmpassword=""
                        />
                    </div>
                    <div className="button-container">
                        <p className="text-primary toggle-button" onClick={this.toggleMode}>
                            <span className="button-text">
                                Already have an account? Log in
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default AuthenticationScreen;
