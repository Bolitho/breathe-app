import React, { Component, ChangeEvent } from 'react';
import { Auth, DataStore } from 'aws-amplify';
import './Reminders.scss';
import { NotificationPreferences } from '../../models';

enum NotificationMode {
    EMAIL = 'EMAIL',
    PUSH = 'PUSH',
    SMS = 'SMS',
}

enum NotificationOccurrence {
    MORNING = 'MORNING',
    AFTERNOON = 'AFTERNOON',
    EVENING = 'EVENING',
}

interface RemindersProps {
    userId: string;
    notificationState: boolean;
    notificationMode: string;
    notificationOccurrence: string;
    notificationTime: string;
}

interface RemindersState extends RemindersProps {}

class Reminders extends Component<RemindersProps, RemindersState> {
    private userId: string = '';

    constructor(props: RemindersProps) {
        super(props);
        this.state = {
            userId: this.userId,
            notificationState: true,
            notificationMode: NotificationMode.EMAIL,
            notificationOccurrence: NotificationOccurrence.MORNING,
            notificationTime: '09:00 AM',
        };
    }

    handleSave = async () => {
        try {
            await DataStore.clear();

            Auth.currentAuthenticatedUser().then(async (currentUser) => {
                this.userId = currentUser.attributes.sub;
                this.setState(
                    (prevState) => ({
                        ...prevState,
                        userId: currentUser.attributes.sub,
                    }),
                    async () => {
                        const notificationPreferences = new NotificationPreferences({
                            ...this.state,
                        });

                        console.log(this.state);
                        console.log(notificationPreferences);

                        try {
                            const preference = await DataStore.save(notificationPreferences);
                          
                            console.log('Preference saved:', preference);
                            const savedPreferences = await DataStore.query(NotificationPreferences);
                            console.log('Saved preferences:', savedPreferences);
                        } catch (error) {
                            if (error instanceof Error) {
                                console.log('preference error: ' + error.message);
                            }
                        }
                    }
                );
            });
        } catch (error) {
            if (error instanceof Error) {
                console.log('preference error: ' + error.message);
            }
        }
    };

    handleInputChange = (
        event: ChangeEvent<HTMLInputElement | HTMLSelectElement>
    ) => {
         
        const { name, value } = event.target;
        this.setState((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    render() {
        const {
            userId,
            notificationState,
            notificationMode,
            notificationOccurrence,
            notificationTime,
        } = this.state;

        return (
            <form className="form">
                <div className="form-group">
                    <label htmlFor="notificationState">Set Notification State</label>
                    <select
                        name="notificationState"
                        id="notificationState"
                        className="form-control"
                        value={notificationState.toString()}
                        onChange={this.handleInputChange}
                    >
                        <option value="true">Enabled</option>
                        <option value="false">Disabled</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="notificationMode">Set Notification Mode</label>
                    <select
                        name="notificationMode"
                        id="notificationMode"
                        className="form-control"
                        value={notificationMode}
                        onChange={this.handleInputChange}
                    >
                        <option value={NotificationMode.EMAIL}>Email</option>
                        <option value={NotificationMode.PUSH}>Push</option>
                        <option value={NotificationMode.SMS}>SMS</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="notificationOccurrence">Set Notification Occurrence</label>
                    <select
                        name="notificationOccurrence"
                        id="notificationOccurrence"
                        className="form-control"
                        value={notificationOccurrence}
                        onChange={this.handleInputChange}
                    >
                        <option value={NotificationOccurrence.MORNING}>Morning</option>
                        <option value={NotificationOccurrence.AFTERNOON}>Afternoon</option>
                        <option value={NotificationOccurrence.EVENING}>Evening</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="notificationTime">Set Notification Time</label>
                    <input
                        type="time"
                        className="form-control"
                        name="notificationTime"
                        value={notificationTime}
                        onChange={this.handleInputChange}
                    />
                </div>
                <button
                    type="button"
                    className="button button-custom"
                    onClick={this.handleSave}
                >
                    Save
                </button>
            </form>
        );
    }
}

export default Reminders;
