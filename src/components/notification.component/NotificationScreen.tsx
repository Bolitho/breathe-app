import React from "react";
import "./NotificationScreen.scss"
import Reminders from "../reminders.component/Reminders";


const NotificationScreen: React.FC = () => {
  return (
    <div className="notification-screen container  d-flex justify-content-center align-items-center vh-10">
        <div className="card">
        <div className="card-header-h2 d-flex justify-content-center"><h2 className="card-header-h2 ">Notification Settings</h2></div>
      <div className="center-content card-body">
        <Reminders userId=""
        notificationMode=""
        notificationOccurrence=""
        notificationState
        notificationTime=""/>
      </div>
        </div>
       
    </div>
  );
};

export default NotificationScreen;
