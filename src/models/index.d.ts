import { ModelInit, MutableModel, __modelMeta__, ManagedIdentifier } from "@aws-amplify/datastore";
// @ts-ignore
import { LazyLoading, LazyLoadingDisabled } from "@aws-amplify/datastore";





type EagerNotificationPreferences = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<NotificationPreferences, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly userId: string;
  readonly notificationState: boolean;
  readonly notificationMode: string;
  readonly notificationOccurrence: string;
  readonly notificationTime: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

type LazyNotificationPreferences = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<NotificationPreferences, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly userId: string;
  readonly notificationState: boolean;
  readonly notificationMode: string;
  readonly notificationOccurrence: string;
  readonly notificationTime: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

export declare type NotificationPreferences = LazyLoading extends LazyLoadingDisabled ? EagerNotificationPreferences : LazyNotificationPreferences

export declare const NotificationPreferences: (new (init: ModelInit<NotificationPreferences>) => NotificationPreferences) & {
  copyOf(source: NotificationPreferences, mutator: (draft: MutableModel<NotificationPreferences>) => MutableModel<NotificationPreferences> | void): NotificationPreferences;
}