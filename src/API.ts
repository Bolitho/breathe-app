/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateNotificationPreferencesInput = {
  id?: string | null,
  userId: string,
  notificationState: boolean,
  notificationMode: string,
  notificationOccurrence: string,
  notificationTime: string,
};

export type ModelNotificationPreferencesConditionInput = {
  userId?: ModelStringInput | null,
  notificationState?: ModelBooleanInput | null,
  notificationMode?: ModelStringInput | null,
  notificationOccurrence?: ModelStringInput | null,
  notificationTime?: ModelStringInput | null,
  and?: Array< ModelNotificationPreferencesConditionInput | null > | null,
  or?: Array< ModelNotificationPreferencesConditionInput | null > | null,
  not?: ModelNotificationPreferencesConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelBooleanInput = {
  ne?: boolean | null,
  eq?: boolean | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type NotificationPreferences = {
  __typename: "NotificationPreferences",
  id?: string | null,
  userId: string,
  notificationState: boolean,
  notificationMode: string,
  notificationOccurrence: string,
  notificationTime: string,
  createdAt: string,
  updatedAt: string,
};

export type UpdateNotificationPreferencesInput = {
  id: string,
  userId?: string | null,
  notificationState?: boolean | null,
  notificationMode?: string | null,
  notificationOccurrence?: string | null,
  notificationTime?: string | null,
};

export type DeleteNotificationPreferencesInput = {
  id: string,
};

export type ModelNotificationPreferencesFilterInput = {
  id?: ModelIDInput | null,
  userId?: ModelStringInput | null,
  notificationState?: ModelBooleanInput | null,
  notificationMode?: ModelStringInput | null,
  notificationOccurrence?: ModelStringInput | null,
  notificationTime?: ModelStringInput | null,
  and?: Array< ModelNotificationPreferencesFilterInput | null > | null,
  or?: Array< ModelNotificationPreferencesFilterInput | null > | null,
  not?: ModelNotificationPreferencesFilterInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type ModelNotificationPreferencesConnection = {
  __typename: "ModelNotificationPreferencesConnection",
  items:  Array<NotificationPreferences | null >,
  nextToken?: string | null,
};

export type ModelSubscriptionNotificationPreferencesFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  userId?: ModelSubscriptionStringInput | null,
  notificationState?: ModelSubscriptionBooleanInput | null,
  notificationMode?: ModelSubscriptionStringInput | null,
  notificationOccurrence?: ModelSubscriptionStringInput | null,
  notificationTime?: ModelSubscriptionStringInput | null,
  and?: Array< ModelSubscriptionNotificationPreferencesFilterInput | null > | null,
  or?: Array< ModelSubscriptionNotificationPreferencesFilterInput | null > | null,
};

export type ModelSubscriptionIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  in?: Array< string | null > | null,
  notIn?: Array< string | null > | null,
};

export type ModelSubscriptionStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  in?: Array< string | null > | null,
  notIn?: Array< string | null > | null,
};

export type ModelSubscriptionBooleanInput = {
  ne?: boolean | null,
  eq?: boolean | null,
};

export type CreateNotificationPreferencesMutationVariables = {
  input: CreateNotificationPreferencesInput,
  condition?: ModelNotificationPreferencesConditionInput | null,
};

export type CreateNotificationPreferencesMutation = {
  createNotificationPreferences?:  {
    __typename: "NotificationPreferences",
    id?: string | null,
    userId: string,
    notificationState: boolean,
    notificationMode: string,
    notificationOccurrence: string,
    notificationTime: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateNotificationPreferencesMutationVariables = {
  input: UpdateNotificationPreferencesInput,
  condition?: ModelNotificationPreferencesConditionInput | null,
};

export type UpdateNotificationPreferencesMutation = {
  updateNotificationPreferences?:  {
    __typename: "NotificationPreferences",
    id?: string | null,
    userId: string,
    notificationState: boolean,
    notificationMode: string,
    notificationOccurrence: string,
    notificationTime: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteNotificationPreferencesMutationVariables = {
  input: DeleteNotificationPreferencesInput,
  condition?: ModelNotificationPreferencesConditionInput | null,
};

export type DeleteNotificationPreferencesMutation = {
  deleteNotificationPreferences?:  {
    __typename: "NotificationPreferences",
    id?: string | null,
    userId: string,
    notificationState: boolean,
    notificationMode: string,
    notificationOccurrence: string,
    notificationTime: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type GetNotificationPreferencesQueryVariables = {
  id: string,
};

export type GetNotificationPreferencesQuery = {
  getNotificationPreferences?:  {
    __typename: "NotificationPreferences",
    id?: string | null,
    userId: string,
    notificationState: boolean,
    notificationMode: string,
    notificationOccurrence: string,
    notificationTime: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListNotificationPreferencesQueryVariables = {
  filter?: ModelNotificationPreferencesFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListNotificationPreferencesQuery = {
  listNotificationPreferences?:  {
    __typename: "ModelNotificationPreferencesConnection",
    items:  Array< {
      __typename: "NotificationPreferences",
      id?: string | null,
      userId: string,
      notificationState: boolean,
      notificationMode: string,
      notificationOccurrence: string,
      notificationTime: string,
      createdAt: string,
      updatedAt: string,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type OnCreateNotificationPreferencesSubscriptionVariables = {
  filter?: ModelSubscriptionNotificationPreferencesFilterInput | null,
};

export type OnCreateNotificationPreferencesSubscription = {
  onCreateNotificationPreferences?:  {
    __typename: "NotificationPreferences",
    id?: string | null,
    userId: string,
    notificationState: boolean,
    notificationMode: string,
    notificationOccurrence: string,
    notificationTime: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateNotificationPreferencesSubscriptionVariables = {
  filter?: ModelSubscriptionNotificationPreferencesFilterInput | null,
};

export type OnUpdateNotificationPreferencesSubscription = {
  onUpdateNotificationPreferences?:  {
    __typename: "NotificationPreferences",
    id?: string | null,
    userId: string,
    notificationState: boolean,
    notificationMode: string,
    notificationOccurrence: string,
    notificationTime: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteNotificationPreferencesSubscriptionVariables = {
  filter?: ModelSubscriptionNotificationPreferencesFilterInput | null,
};

export type OnDeleteNotificationPreferencesSubscription = {
  onDeleteNotificationPreferences?:  {
    __typename: "NotificationPreferences",
    id?: string | null,
    userId: string,
    notificationState: boolean,
    notificationMode: string,
    notificationOccurrence: string,
    notificationTime: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};
