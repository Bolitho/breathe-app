import React from 'react';
import './App.css';
import AuthenticationScreen from './components/authentication.component/AuthenticationScreen';
import Header from './components/header.component/Header';
import { BrowserRouter as Router } from 'react-router-dom';
import { Amplify } from 'aws-amplify';
import awsconfig from './aws-exports';  
Amplify.configure(awsconfig);


const App: React.FC = () => {
     
    return (
        <Router>
            <Header /> 
            <div className='container'>
                <AuthenticationScreen handleLoginStatus={()=>{}} />
            </div> 
        </Router>
    );
}

export default App;
